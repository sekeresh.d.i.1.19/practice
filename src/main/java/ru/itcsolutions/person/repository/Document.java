package ru.itcsolutions.person.repository;

/**
 * Документ, удостоверяющий личность
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
public class Document {
    private String docName;
    private String docType;
    private String docSeries;

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocSeries() {
        return docSeries;
    }

    public void setDocSeries(String docSeries) {
        this.docSeries = docSeries;
    }
}
