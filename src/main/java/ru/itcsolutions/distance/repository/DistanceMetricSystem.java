package ru.itcsolutions.distance.repository;

/**
 * Метрическая система
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
public interface DistanceMetricSystem {
    public Double getConvertIndexFromMetres();
    public String getMetricSystemName();
    public Double convertFromMetres(Double metresAmount);
    public Double convertFromKilometres(Double kilometresAmount);
}
