package ru.itcsolutions.currencies.repository.model;

import ru.itcsolutions.currencies.repository.Currency;

/**
 * Доллары
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
public class USDCurrency implements Currency {

    private int usdCodeNum;
    private String usdCode;
    private Double course;

    public USDCurrency() {
        this.course = new Double(1);
        this.usdCodeNum = 840;
        this.usdCode = "USD";

    }

    @Override
    public int getCurrencyCodeNum() {
        return this.usdCodeNum;
    }

    @Override
    public Double getCourseFromUSD() {
        return this.course;
    }

    @Override
    public String getCurrencyCode() {
        return this.usdCode;
    }

    @Override
    public Double convertFromUSD(Double usdAmount) {
        return usdAmount;
    }
}
