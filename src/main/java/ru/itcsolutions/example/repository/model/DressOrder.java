package ru.itcsolutions.example.repository.model;

import ru.itcsolutions.example.repository.ExampleOrder;

import java.time.LocalDateTime;

/**
 * Пример заказа - Одежда
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
public class DressOrder implements ExampleOrder {
    private Long dressOrderId;
    private String dressName;
    private LocalDateTime dressUpdateTime;
    private int dressCost;
    private boolean dressIsInStock;

    public DressOrder(Long orderId, String name, LocalDateTime updateTime, int cost, boolean isInStock) {
        this.dressOrderId = orderId;
        this.dressName = name;
        this.dressUpdateTime = updateTime;
        this.dressCost = cost;
        this.dressIsInStock = isInStock;
    }

    @Override
    public Long getOrderId() {
        return dressOrderId;
    }

    @Override
    public String getName() {
        return dressName;
    }

    @Override
    public LocalDateTime getUpdateTime() {
        return dressUpdateTime;
    }

    @Override
    public int getCost() {
        return dressCost;
    }

    @Override
    public boolean isInStock() {
        return dressIsInStock;
    }

    @Override
    public void setUpdateTime(LocalDateTime localDateTime) {
        this.dressUpdateTime = localDateTime;
    }

    @Override
    public void setOrderId(Long orderId) {
        this.dressOrderId = orderId;
    }
}
