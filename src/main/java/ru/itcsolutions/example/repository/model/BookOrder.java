package ru.itcsolutions.example.repository.model;

import ru.itcsolutions.example.repository.ExampleOrder;

import java.time.LocalDateTime;

/**
 * Пример заказа - Книга
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
public class BookOrder implements ExampleOrder {
    private Long bookOrderId;
    private String bookName;
    private LocalDateTime bookUpdateTime;
    private int bookCost;
    private boolean bookIsInStock;

    public BookOrder(Long orderId, String name, LocalDateTime updateTime, int cost, boolean isInStock) {
        this.bookOrderId = orderId;
        this.bookName = name;
        this.bookUpdateTime = updateTime;
        this.bookCost = cost;
        this.bookIsInStock = isInStock;
    }

    @Override
    public Long getOrderId() {
        return bookOrderId;
    }

    @Override
    public String getName() {
        return bookName;
    }

    @Override
    public LocalDateTime getUpdateTime() {
        return bookUpdateTime;
    }

    @Override
    public int getCost() {
        return bookCost;
    }

    @Override
    public boolean isInStock() {
        return bookIsInStock;
    }

    @Override
    public void setUpdateTime(LocalDateTime localDateTime) {
        this.bookUpdateTime = localDateTime;
    }

    @Override
    public void setOrderId(Long orderId) {
        this.bookOrderId = orderId;
    }
}
